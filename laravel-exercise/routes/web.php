<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\KritikController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'main']);

Route::get('/register', [AuthController::class, 'getRegister']);
Route::post('/welcome', [AuthController::class, 'postRegister']);

Route::get('/welcome', [AuthController::class, 'welcome']);

Route::get('/data-tables', [IndexController::class, 'getTable']);

Route::get('/cast', [CastController::class, 'index'])->name('castIndex');

Route::get('/cast/create', [CastController::class, 'create']);

Route::get('/cast/{cast_id}', [CastController::class, 'show']);

Route::post('/cast', [CastController::class, 'store']);

Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

Route::put('/cast/{cast_id}', [CastController::class, 'update']);

Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth'])->group(function () {
  Route::resource('profile', ProfileController::class)->only(['index', 'update']);

  Route::post('/kritik/{film_id}', [KritikController::class, 'store']);
});

Route::resource('genre', GenreController::class);
Route::resource('film', FilmController::class);


