<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function main() {
        return view('front');
    }

    public function getTable() {
        return view('table');
    }
}
