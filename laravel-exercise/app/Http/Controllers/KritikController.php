<?php

namespace App\Http\Controllers;

use App\Models\Kritik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KritikController extends Controller
{
    public function store(Request $request, $id)
    {
        $request->validate([
            'content' => 'required',
            'point' => 'required|integer|min:1|max:10',
        ]);

        $kritik = new Kritik();
        $kritik->user_id = Auth::id();
        $kritik->film_id = $id;
        $kritik->content = $request->input('content');
        $kritik->point = $request->input('point');
        $kritik->save();

        return redirect('/film/' .$id);
    }
}
