<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $idUser = Auth::id();

        $detailProfile = Profile::where('user_id', $idUser)->first();

        return view('profile.index', ['detailProfile' => $detailProfile]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
        ]);

        $profile = Profile::find($id);

        $profile->umur = $request->umur;
        $profile->bio = $request->bio;
        $profile->alamat = $request->alamat;

        $profile->save();

        return redirect('/profile');
    }
}
