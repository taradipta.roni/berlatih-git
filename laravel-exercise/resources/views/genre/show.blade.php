@extends('layout.master')

@section('title')
 Detail Genre {{$genre ? $genre->id : "Tidak tersedia"}}
@endsection

@section('content')
<h4>{{$genre ? $genre->nama : "Tidak tersedia"}}</h4>

<div class="row">
@forelse ($genre->film as $film)
  <div class="col-md-3">
    <div class="card p-3">
      <img src="{{asset('images/' . $film->poster)}}" alt="poster" class="img-fluid" style="height: 200px; object-fit: cover;">
      <h5>{{$film->judul}}</h5>
      <p>{{$film->ringkasan}}</p>
      <a href="/film/{{$film->id}}" class="btn btn-primary">Detail Film</a>
    </div>
  </div>
@empty
  <p>Tidak ada film dengan genre {{$genre->nama}}</p>
@endforelse
</div>
@endsection