@extends('layout.master')

@section('title')
 Edit Genre {{ optional($genre)->id }}
@endsection

@section('content')
<div>
  <h2></h2>
  <form action="/genre/{{ optional($genre)->id }}" method="POST">
      @csrf
      @method('PUT')
      <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" name="nama" value="{{ optional($genre)->nama }}" id="nama" placeholder="Masukkan nama">
          @error('nama')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>
      <button type="submit" class="btn btn-primary">Edit</button>
  </form>
</div>
@endsection
