@extends('layout.master')

@section('title')
  Daftar Films
@endsection

@section('content')
<a href="/film/create" class="btn btn-primary mb-3">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Poster</th>
        <th scope="col">Judul</th>
        <th scope="col">Ringkasan</th>
        <th scope="col">Tahun</th>
        <th scope="col">Genre</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($film as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td style="width: 200px;"><img class="img-fluid" src="{{asset('images/' . $value->poster)}}" alt=""></td>
                <td>{{$value->judul}}</td>
                <td>{{$value->ringkasan}}</td>
                <td>{{$value->tahun}}</td>
                <td>{{$value->genre->nama}}</td>
                <td>
                    <a href="/film/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/film/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <form action="/film/{{$value->id}}" method="POST" class="form-inline d-inline">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection