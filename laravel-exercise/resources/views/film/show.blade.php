@extends('layout.master')

@section('title')
 Detail Film
@endsection

@section('content')

<img src="{{asset('images/' . $film->poster)}}" alt="poster" class="img-fluid mb-3" style="width: 500px;">
<h4 class="font-weight-bold">{{$film->judul}} ({{$film->tahun}})</h4>
<p>{{$film->genre->nama}}</p>
<p>{{$film->ringkasan}}</p>

<div class="card direct-chat direct-chat-primary mt-5">
  <div class="card-header ui-sortable-handle" style="cursor: move;">
    <h3 class="card-title">List Komentar</h3>
  </div>
  <div class="card-body">
    <div class="direct-chat-messages">
      @forelse ($film->kritiks as $kritik)
      <div class="direct-chat-msg">
        <div class="direct-chat-infos clearfix">
          <span class="direct-chat-name float-left">{{ $kritik->user->name }}</span>
          <span class="ml-3 inline-block">
            <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 576 512"><!--! Font Awesome Free 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><style>svg{fill:#ffd814}</style><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z"/></svg>
            {{ $kritik->point }}
          </span>
          <span class="direct-chat-timestamp float-right">{{ $kritik->created_at->format('d M Y H:i a') }}</span>
        </div>
        <div class="bg-light p-3 rounded">
          {{ $kritik->content }}
        </div>
      </div>
      @empty
        <p>Tidak ada komentar</p>
      @endforelse
    </div>
  </div>
  <div class="card-footer">
    <form action="/kritik/{{$film->id}}" method="post">
      @csrf
        <div class="form-group">
            <label for="content">Kritik</label>
            <textarea class="form-control" name="content" id="content" placeholder="Masukkan kritik"></textarea>
            @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="point">Point</label>
            <select class="form-control" name="point" id="point">
                @for($i = 1; $i <= 10; $i++)
                    <option value="{{ $i }}">{{ $i }}</option>
                @endfor
            </select>
            @error('point')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
</div>


@endsection