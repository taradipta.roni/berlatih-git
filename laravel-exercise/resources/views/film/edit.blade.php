@extends('layout.master')

@section('title')
  Edit Film {{ $film->id }}
@endsection

@section('content')
<div>
  <h2></h2>
  <form action="/film/{{ $film->id }}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="form-group">
          <label for="judul">Judul</label>
          <input type="text" class="form-control" name="judul" value="{{ $film->judul }}" id="judul" placeholder="Masukkan judul">
          @error('judul')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>
      <div class="form-group">
          <label for="ringkasan">Ringkasan</label>
          <textarea class="form-control" name="ringkasan" id="ringkasan" placeholder="Masukkan ringkasan">{{ $film->ringkasan }}</textarea>
          @error('ringkasan')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>
      <div class="form-group">
          <label for="tahun">Tahun</label>
          <input type="text" class="form-control" name="tahun"  value="{{ $film->tahun }}"  id="tahun" placeholder="Masukkan tahun">
          @error('tahun')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>
      <div class="form-group">
          <label for="poster">Poster</label>
          <input type="file" class="form-control" name="poster" id="poster">
          @error('poster')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
          <img src="{{ asset('images/'.$film->poster) }}" alt="Poster Film" width="200" class="my-3">
      </div>
      <div class="form-group">
        <label for="genre">Genre</label>
        <select class="form-control" name="genre_id" id="genre" required>
            @foreach($genres as $genre)
              <option value="{{ $genre->id }}" {{ $film->genre_id == $genre->id ? 'selected' : '' }}>{{ $genre->nama }}</option>
            @endforeach
        </select>
        @error('genre_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
      <button type="submit" class="btn btn-primary">Edit</button>
  </form>
</div>
@endsection
