@extends('layout.master')

@section('title')
 Detail Cast {{$cast ? $cast->id : "Tidak tersedia"}}
@endsection

@section('content')
<h4>{{$cast ? $cast->nama : "Tidak tersedia"}}</h4>
<p>{{$cast ? $cast->umur : "Tidak tersedia"}}</p>
<p>{{$cast ? $cast->bio : "Tidak tersedia"}}</p>
@endsection