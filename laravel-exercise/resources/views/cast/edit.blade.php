@extends('layout.master')

@section('title')
 Edit Cast {{ optional($cast)->id }}
@endsection

@section('content')
<div>
  <h2></h2>
  <form action="/cast/{{ optional($cast)->id }}" method="POST">
      @csrf
      @method('PUT')
      <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" name="nama" value="{{ optional($cast)->nama }}" id="nama" placeholder="Masukkan nama">
          @error('nama')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>
      <div class="form-group">
          <label for="umur">Umur</label>
          <input type="text" class="form-control" name="umur"  value="{{ optional($cast)->umur }}"  id="umur" placeholder="Masukkan umur">
          @error('umur')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>
      <div class="form-group">
          <label for="bio">Bio</label>
          <textarea class="form-control" name="bio" id="bio" placeholder="Masukkan bio">{{ optional($cast)->bio }}</textarea>
          @error('bio')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>
      <button type="submit" class="btn btn-primary">Edit</button>
  </form>
</div>
@endsection
