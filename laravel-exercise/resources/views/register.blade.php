@extends('layout.master')

@section('title')
    Halaman Form
@endsection

@section('content')
    <h2>Buat Account Baru</h2>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
      @csrf
      <label for="firstname">First name :</label><br />
      <input type="text" id="firstname" name="firstname" required/><br /><br />
      <label for="lastname">Last name :</label><br />
      <input type="text" id="lastname" name="lastname" required/><br /><br />
      <p>Gender</p>
      <input type="radio" id="male" value="male" name="gender" />
      <label for="male">Male</label><br />
      <input type="radio" id="female" value="female" name="gender" />
      <label for="female">Female</label><br /><br />
      <label for="nationality">Nationality</label><br />
      <select id="nationality" name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="malaysia">Malaysia</option>
        <option value="singapura">Singapura</option>
      </select>
      <p>Language Spoken</p>
      <input type="checkbox" id="indonesia" value="indonesia" name="language[]" />
      <label for="indonesia">Bahasa Indonesia</label><br />
      <input type="checkbox" id="english" value="english" name="language[]" />
      <label for="english">English</label><br />
      <input type="checkbox" id="other" value="other" name="language[]" />
      <label for="other">Other</label><br /><br />
      <label>Bio</label><br />
      <textarea rows="8" cols="40" name="bio"></textarea><br />
      <button type="submit">Sign Up</button>
    </form>
@endsection
    
 
