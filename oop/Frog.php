<?php

require_once("Animal.php");

class Frog extends Animal {
  public function jump() {
    echo "hop hop";
  }

  public function print_frog() {
    $this->print_info();
    echo "Jump : ";
    $this->jump();
    echo "<br>";
  }
}