<?php

require_once("Animal.php");
require_once("Ape.php");
require_once("Frog.php");

// Release 0
echo "<h2>Release 0</h2>";

$sheep = new Animal("shaun");

echo $sheep->name . "<br>"; // "shaun"
echo $sheep->legs . "<br>"; // 4
echo $sheep->cold_blooded . "<br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
echo "<br>";
echo $sheep->get_name() . "<br>";
echo $sheep->get_legs() . "<br>";
echo $sheep->get_cold_blooded() . "<br>";


// Release 1
echo "<h2>Release 1</h2>";

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
echo "<br>";
$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"

// output akhir
echo "<h2>Output Akhir</h2>";
$sheep->print_info();
echo "<br>";
$kodok->print_frog();
echo "<br>";
$sungokong->print_ape();